source ~/bin/antigen.zsh

antigen use oh-my-zsh
antigen theme terminalparty
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search
antigen bundle tarruda/zsh-autosuggestions
antigen bundle git
antigen bundle ruby
antigen bundle tmux
antigen bundle robbyrussell/oh-my-zsh plugins/rbenv

if [[ $(uname) = Darwin ]]; then
	antigen bundle osx
	antigen bundle textmate
	antigen bundle brew
fi

antigen apply
